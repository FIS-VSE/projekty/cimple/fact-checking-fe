# Fact-checking front-end prototype

The software prototype of the front-end component for supporting information verification, written in the React framework, is based on the results of a focus group conducted with respondents from the field of information verification within the CIMPLE project. 

This software supports the information verification process during the selection of statements to verify, gathering supporting materials, and formulating explanations of the verdict. It utilizes a range of artificial intelligence services that are the focus of research teams involved in the project. 

These services include identifying statements suitable for verification based on the full text of documents, recommending relevant academic articles and researchers, finding similar statements that have already been verified, automatically summarizing explanations of verdicts, and generating supportive media to facilitate sharing.

## Installation and Running

To install and run the application, follow these steps:

### Cloning the Repository:

```bash
git clone [repository_URL]
cd [repository_name]
```

### Installing Dependencies:

```bash
npm install
```

### Running the Application:

```bash
npm start
```

### Building the Application for Production:

```bash
npm run build
```

## Using the Application

### Login

Users can log in using their credentials. After logging in, they have access to advanced features such as managing claims and evidence.

### Viewing Claims

- On the `All Claims` page, users can view all available claims.
- To view the details of a claim, users can click on a specific claim, which will redirect them to the `Article Claims` page.

### Managing Evidence

- The `Evidence Retrieval` page allows users to view and manage evidence related to claims.
- Users can add new evidence or edit existing ones.

### Summarization

- The `Summarization` page provides tools for summarizing claims and evidence.
- Users can set different criteria for summarization using the `SumSettingsPopover` component.

## API Communication

The application communicates with the CIMPLE Data API to retrieve, create, update, and delete claims, evidence, experts, and users. Each of these operations is performed using the respective HTTP methods (GET, POST, PUT, DELETE).

A mockup of API can be found in the `backend` folder.
